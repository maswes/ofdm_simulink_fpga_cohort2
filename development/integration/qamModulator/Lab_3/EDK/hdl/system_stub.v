//-----------------------------------------------------------------------------
// system_stub.v
//-----------------------------------------------------------------------------

module system_stub
  (
    processing_system7_0_MIO,
    processing_system7_0_PS_SRSTB_pin,
    processing_system7_0_PS_CLK_pin,
    processing_system7_0_PS_PORB_pin,
    processing_system7_0_DDR_Clk,
    processing_system7_0_DDR_Clk_n,
    processing_system7_0_DDR_CKE,
    processing_system7_0_DDR_CS_n,
    processing_system7_0_DDR_RAS_n,
    processing_system7_0_DDR_CAS_n,
    processing_system7_0_DDR_WEB_pin,
    processing_system7_0_DDR_BankAddr,
    processing_system7_0_DDR_Addr,
    processing_system7_0_DDR_ODT,
    processing_system7_0_DDR_DRSTB,
    processing_system7_0_DDR_DQ,
    processing_system7_0_DDR_DM,
    processing_system7_0_DDR_DQS,
    processing_system7_0_DDR_DQS_n,
    processing_system7_0_DDR_VRN,
    processing_system7_0_DDR_VRP,
    axi_gpio_button_GPIO_IO_I_pin,
    axi_gpio_led_GPIO_IO_pin,
    axi_gpio_led_GPIO2_IO_pin,
    dac_driver_tx_iq_sel_pin,
    dac_driver_txd_pin,
    dac_driver_blinky_pin,
    mcu_driver_init_done_pin,
    mcu_driver_mcu_reset_out_pin,
    mcu_driver_tr_sw_pin,
    mcu_driver_pa_en_pin,
    mcu_driver_tx_en_pin,
    mcu_driver_rx_en_pin,
    mcu_driver_blinky_pin,
    axi_gpio_switch_GPIO_IO_I_pin,
    mcu_uart_RX_pin,
    mcu_uart_TX_pin,
    tx_clock_generator_CLKIN_pin,
    tx_clock_generator_rx_clk_pin,
    tx_clock_generator_tx_clk_pin,
    tx_clock_generator_LOCKED_pin
  );
  inout [53:0] processing_system7_0_MIO;
  input processing_system7_0_PS_SRSTB_pin;
  input processing_system7_0_PS_CLK_pin;
  input processing_system7_0_PS_PORB_pin;
  inout processing_system7_0_DDR_Clk;
  inout processing_system7_0_DDR_Clk_n;
  inout processing_system7_0_DDR_CKE;
  inout processing_system7_0_DDR_CS_n;
  inout processing_system7_0_DDR_RAS_n;
  inout processing_system7_0_DDR_CAS_n;
  output processing_system7_0_DDR_WEB_pin;
  inout [2:0] processing_system7_0_DDR_BankAddr;
  inout [14:0] processing_system7_0_DDR_Addr;
  inout processing_system7_0_DDR_ODT;
  inout processing_system7_0_DDR_DRSTB;
  inout [31:0] processing_system7_0_DDR_DQ;
  inout [3:0] processing_system7_0_DDR_DM;
  inout [3:0] processing_system7_0_DDR_DQS;
  inout [3:0] processing_system7_0_DDR_DQS_n;
  inout processing_system7_0_DDR_VRN;
  inout processing_system7_0_DDR_VRP;
  input axi_gpio_button_GPIO_IO_I_pin;
  inout axi_gpio_led_GPIO_IO_pin;
  inout axi_gpio_led_GPIO2_IO_pin;
  output dac_driver_tx_iq_sel_pin;
  output [11:0] dac_driver_txd_pin;
  output dac_driver_blinky_pin;
  input mcu_driver_init_done_pin;
  output mcu_driver_mcu_reset_out_pin;
  output mcu_driver_tr_sw_pin;
  output mcu_driver_pa_en_pin;
  output mcu_driver_tx_en_pin;
  output mcu_driver_rx_en_pin;
  output mcu_driver_blinky_pin;
  input axi_gpio_switch_GPIO_IO_I_pin;
  input mcu_uart_RX_pin;
  output mcu_uart_TX_pin;
  input tx_clock_generator_CLKIN_pin;
  output tx_clock_generator_rx_clk_pin;
  output tx_clock_generator_tx_clk_pin;
  output tx_clock_generator_LOCKED_pin;

  (* BOX_TYPE = "user_black_box" *)
  system
    system_i (
      .processing_system7_0_MIO ( processing_system7_0_MIO ),
      .processing_system7_0_PS_SRSTB_pin ( processing_system7_0_PS_SRSTB_pin ),
      .processing_system7_0_PS_CLK_pin ( processing_system7_0_PS_CLK_pin ),
      .processing_system7_0_PS_PORB_pin ( processing_system7_0_PS_PORB_pin ),
      .processing_system7_0_DDR_Clk ( processing_system7_0_DDR_Clk ),
      .processing_system7_0_DDR_Clk_n ( processing_system7_0_DDR_Clk_n ),
      .processing_system7_0_DDR_CKE ( processing_system7_0_DDR_CKE ),
      .processing_system7_0_DDR_CS_n ( processing_system7_0_DDR_CS_n ),
      .processing_system7_0_DDR_RAS_n ( processing_system7_0_DDR_RAS_n ),
      .processing_system7_0_DDR_CAS_n ( processing_system7_0_DDR_CAS_n ),
      .processing_system7_0_DDR_WEB_pin ( processing_system7_0_DDR_WEB_pin ),
      .processing_system7_0_DDR_BankAddr ( processing_system7_0_DDR_BankAddr ),
      .processing_system7_0_DDR_Addr ( processing_system7_0_DDR_Addr ),
      .processing_system7_0_DDR_ODT ( processing_system7_0_DDR_ODT ),
      .processing_system7_0_DDR_DRSTB ( processing_system7_0_DDR_DRSTB ),
      .processing_system7_0_DDR_DQ ( processing_system7_0_DDR_DQ ),
      .processing_system7_0_DDR_DM ( processing_system7_0_DDR_DM ),
      .processing_system7_0_DDR_DQS ( processing_system7_0_DDR_DQS ),
      .processing_system7_0_DDR_DQS_n ( processing_system7_0_DDR_DQS_n ),
      .processing_system7_0_DDR_VRN ( processing_system7_0_DDR_VRN ),
      .processing_system7_0_DDR_VRP ( processing_system7_0_DDR_VRP ),
      .axi_gpio_button_GPIO_IO_I_pin ( axi_gpio_button_GPIO_IO_I_pin ),
      .axi_gpio_led_GPIO_IO_pin ( axi_gpio_led_GPIO_IO_pin ),
      .axi_gpio_led_GPIO2_IO_pin ( axi_gpio_led_GPIO2_IO_pin ),
      .dac_driver_tx_iq_sel_pin ( dac_driver_tx_iq_sel_pin ),
      .dac_driver_txd_pin ( dac_driver_txd_pin ),
      .dac_driver_blinky_pin ( dac_driver_blinky_pin ),
      .mcu_driver_init_done_pin ( mcu_driver_init_done_pin ),
      .mcu_driver_mcu_reset_out_pin ( mcu_driver_mcu_reset_out_pin ),
      .mcu_driver_tr_sw_pin ( mcu_driver_tr_sw_pin ),
      .mcu_driver_pa_en_pin ( mcu_driver_pa_en_pin ),
      .mcu_driver_tx_en_pin ( mcu_driver_tx_en_pin ),
      .mcu_driver_rx_en_pin ( mcu_driver_rx_en_pin ),
      .mcu_driver_blinky_pin ( mcu_driver_blinky_pin ),
      .axi_gpio_switch_GPIO_IO_I_pin ( axi_gpio_switch_GPIO_IO_I_pin ),
      .mcu_uart_RX_pin ( mcu_uart_RX_pin ),
      .mcu_uart_TX_pin ( mcu_uart_TX_pin ),
      .tx_clock_generator_CLKIN_pin ( tx_clock_generator_CLKIN_pin ),
      .tx_clock_generator_rx_clk_pin ( tx_clock_generator_rx_clk_pin ),
      .tx_clock_generator_tx_clk_pin ( tx_clock_generator_tx_clk_pin ),
      .tx_clock_generator_LOCKED_pin ( tx_clock_generator_LOCKED_pin )
    );

endmodule

module system
  (
    processing_system7_0_MIO,
    processing_system7_0_PS_SRSTB_pin,
    processing_system7_0_PS_CLK_pin,
    processing_system7_0_PS_PORB_pin,
    processing_system7_0_DDR_Clk,
    processing_system7_0_DDR_Clk_n,
    processing_system7_0_DDR_CKE,
    processing_system7_0_DDR_CS_n,
    processing_system7_0_DDR_RAS_n,
    processing_system7_0_DDR_CAS_n,
    processing_system7_0_DDR_WEB_pin,
    processing_system7_0_DDR_BankAddr,
    processing_system7_0_DDR_Addr,
    processing_system7_0_DDR_ODT,
    processing_system7_0_DDR_DRSTB,
    processing_system7_0_DDR_DQ,
    processing_system7_0_DDR_DM,
    processing_system7_0_DDR_DQS,
    processing_system7_0_DDR_DQS_n,
    processing_system7_0_DDR_VRN,
    processing_system7_0_DDR_VRP,
    axi_gpio_button_GPIO_IO_I_pin,
    axi_gpio_led_GPIO_IO_pin,
    axi_gpio_led_GPIO2_IO_pin,
    dac_driver_tx_iq_sel_pin,
    dac_driver_txd_pin,
    dac_driver_blinky_pin,
    mcu_driver_init_done_pin,
    mcu_driver_mcu_reset_out_pin,
    mcu_driver_tr_sw_pin,
    mcu_driver_pa_en_pin,
    mcu_driver_tx_en_pin,
    mcu_driver_rx_en_pin,
    mcu_driver_blinky_pin,
    axi_gpio_switch_GPIO_IO_I_pin,
    mcu_uart_RX_pin,
    mcu_uart_TX_pin,
    tx_clock_generator_CLKIN_pin,
    tx_clock_generator_rx_clk_pin,
    tx_clock_generator_tx_clk_pin,
    tx_clock_generator_LOCKED_pin
  );
  inout [53:0] processing_system7_0_MIO;
  input processing_system7_0_PS_SRSTB_pin;
  input processing_system7_0_PS_CLK_pin;
  input processing_system7_0_PS_PORB_pin;
  inout processing_system7_0_DDR_Clk;
  inout processing_system7_0_DDR_Clk_n;
  inout processing_system7_0_DDR_CKE;
  inout processing_system7_0_DDR_CS_n;
  inout processing_system7_0_DDR_RAS_n;
  inout processing_system7_0_DDR_CAS_n;
  output processing_system7_0_DDR_WEB_pin;
  inout [2:0] processing_system7_0_DDR_BankAddr;
  inout [14:0] processing_system7_0_DDR_Addr;
  inout processing_system7_0_DDR_ODT;
  inout processing_system7_0_DDR_DRSTB;
  inout [31:0] processing_system7_0_DDR_DQ;
  inout [3:0] processing_system7_0_DDR_DM;
  inout [3:0] processing_system7_0_DDR_DQS;
  inout [3:0] processing_system7_0_DDR_DQS_n;
  inout processing_system7_0_DDR_VRN;
  inout processing_system7_0_DDR_VRP;
  input axi_gpio_button_GPIO_IO_I_pin;
  inout axi_gpio_led_GPIO_IO_pin;
  inout axi_gpio_led_GPIO2_IO_pin;
  output dac_driver_tx_iq_sel_pin;
  output [11:0] dac_driver_txd_pin;
  output dac_driver_blinky_pin;
  input mcu_driver_init_done_pin;
  output mcu_driver_mcu_reset_out_pin;
  output mcu_driver_tr_sw_pin;
  output mcu_driver_pa_en_pin;
  output mcu_driver_tx_en_pin;
  output mcu_driver_rx_en_pin;
  output mcu_driver_blinky_pin;
  input axi_gpio_switch_GPIO_IO_I_pin;
  input mcu_uart_RX_pin;
  output mcu_uart_TX_pin;
  input tx_clock_generator_CLKIN_pin;
  output tx_clock_generator_rx_clk_pin;
  output tx_clock_generator_tx_clk_pin;
  output tx_clock_generator_LOCKED_pin;
endmodule

