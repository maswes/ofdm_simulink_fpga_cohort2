The following files were generated for 'chipscope_ila_1' in directory
E:\School\Chilipepper\Labs\Lab_3\EDK\implementation\chipscope_ila_1_wrapper\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_ila_1.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_ila_1.cdc
   * chipscope_ila_1.constraints/chipscope_ila_1.ucf
   * chipscope_ila_1.constraints/chipscope_ila_1.xdc
   * chipscope_ila_1.ncf
   * chipscope_ila_1.ngc
   * chipscope_ila_1.ucf
   * chipscope_ila_1.xdc
   * chipscope_ila_1_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_ila_1.asy

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * chipscope_ila_1.gise
   * chipscope_ila_1.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_ila_1_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_ila_1_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

