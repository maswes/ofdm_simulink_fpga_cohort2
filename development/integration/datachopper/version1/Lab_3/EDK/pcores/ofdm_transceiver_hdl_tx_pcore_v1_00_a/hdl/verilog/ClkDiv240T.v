// -------------------------------------------------------------
// 
// File Name: hdl_prj\hdlsrc\OFDM_Transceiver_HDL\ClkDiv240T.v
// Created: 2014-05-23 21:48:31
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: ClkDiv240T
// Source Path: OFDM_Transceiver_HDL/Tx/DataChopper/ClkDiv240T
// Hierarchy Level: 2
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module ClkDiv240T
          (
           clk,
           rst,
           enb,
           Out1
          );


  input   clk;
  input   rst;
  input   enb;
  output  Out1;


  reg [7:0] Ct239T_count;  // ufix8
  wire [7:0] Ct239T_out1;  // uint8
  wire Tick239T_out1;


  // Count limited, Unsigned Counter
  //  initial value   = 0
  //  step value      = 1
  //  count to value  = 239
  always @(posedge clk or posedge rst)
    begin : Ct239T_process
      if (rst == 1'b1) begin
        Ct239T_count <= 8'b00000000;
      end
      else begin
        if (enb) begin
          if (Ct239T_count == 8'b11101111) begin
            Ct239T_count <= 8'b00000000;
          end
          else begin
            Ct239T_count <= Ct239T_count + 1;
          end
        end
      end
    end

  assign Ct239T_out1 = Ct239T_count;



  assign Tick239T_out1 = (Ct239T_out1 == 8'b00000000 ? 1'b1 :
              1'b0);



  assign Out1 = Tick239T_out1;

endmodule  // ClkDiv240T

