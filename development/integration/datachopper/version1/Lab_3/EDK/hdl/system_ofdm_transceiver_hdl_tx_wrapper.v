//-----------------------------------------------------------------------------
// system_ofdm_transceiver_hdl_tx_wrapper.v
//-----------------------------------------------------------------------------

module system_ofdm_transceiver_hdl_tx_wrapper
  (
    IPCORE_CLK,
    IPCORE_RESETN,
    AXI_Lite_ACLK,
    AXI_Lite_ARESETN,
    AXI_Lite_AWADDR,
    AXI_Lite_AWVALID,
    AXI_Lite_AWREADY,
    AXI_Lite_WDATA,
    AXI_Lite_WSTRB,
    AXI_Lite_WVALID,
    AXI_Lite_WREADY,
    AXI_Lite_BRESP,
    AXI_Lite_BVALID,
    AXI_Lite_BREADY,
    AXI_Lite_ARADDR,
    AXI_Lite_ARVALID,
    AXI_Lite_ARREADY,
    AXI_Lite_RDATA,
    AXI_Lite_RRESP,
    AXI_Lite_RVALID,
    AXI_Lite_RREADY,
    rst_1,
    data_in,
    empty,
    q_out,
    i_out,
    request_byte
  );
  input IPCORE_CLK;
  input IPCORE_RESETN;
  input AXI_Lite_ACLK;
  input AXI_Lite_ARESETN;
  input [31:0] AXI_Lite_AWADDR;
  input AXI_Lite_AWVALID;
  output AXI_Lite_AWREADY;
  input [31:0] AXI_Lite_WDATA;
  input [3:0] AXI_Lite_WSTRB;
  input AXI_Lite_WVALID;
  output AXI_Lite_WREADY;
  output [1:0] AXI_Lite_BRESP;
  output AXI_Lite_BVALID;
  input AXI_Lite_BREADY;
  input [31:0] AXI_Lite_ARADDR;
  input AXI_Lite_ARVALID;
  output AXI_Lite_ARREADY;
  output [31:0] AXI_Lite_RDATA;
  output [1:0] AXI_Lite_RRESP;
  output AXI_Lite_RVALID;
  input AXI_Lite_RREADY;
  input rst_1;
  input [7:0] data_in;
  input empty;
  output [11:0] q_out;
  output [11:0] i_out;
  output request_byte;

  ofdm_transceiver_hdl_tx_pcore
    ofdm_transceiver_hdl_tx (
      .IPCORE_CLK ( IPCORE_CLK ),
      .IPCORE_RESETN ( IPCORE_RESETN ),
      .AXI_Lite_ACLK ( AXI_Lite_ACLK ),
      .AXI_Lite_ARESETN ( AXI_Lite_ARESETN ),
      .AXI_Lite_AWADDR ( AXI_Lite_AWADDR ),
      .AXI_Lite_AWVALID ( AXI_Lite_AWVALID ),
      .AXI_Lite_AWREADY ( AXI_Lite_AWREADY ),
      .AXI_Lite_WDATA ( AXI_Lite_WDATA ),
      .AXI_Lite_WSTRB ( AXI_Lite_WSTRB ),
      .AXI_Lite_WVALID ( AXI_Lite_WVALID ),
      .AXI_Lite_WREADY ( AXI_Lite_WREADY ),
      .AXI_Lite_BRESP ( AXI_Lite_BRESP ),
      .AXI_Lite_BVALID ( AXI_Lite_BVALID ),
      .AXI_Lite_BREADY ( AXI_Lite_BREADY ),
      .AXI_Lite_ARADDR ( AXI_Lite_ARADDR ),
      .AXI_Lite_ARVALID ( AXI_Lite_ARVALID ),
      .AXI_Lite_ARREADY ( AXI_Lite_ARREADY ),
      .AXI_Lite_RDATA ( AXI_Lite_RDATA ),
      .AXI_Lite_RRESP ( AXI_Lite_RRESP ),
      .AXI_Lite_RVALID ( AXI_Lite_RVALID ),
      .AXI_Lite_RREADY ( AXI_Lite_RREADY ),
      .rst_1 ( rst_1 ),
      .data_in ( data_in ),
      .empty ( empty ),
      .q_out ( q_out ),
      .i_out ( i_out ),
      .request_byte ( request_byte )
    );

endmodule

