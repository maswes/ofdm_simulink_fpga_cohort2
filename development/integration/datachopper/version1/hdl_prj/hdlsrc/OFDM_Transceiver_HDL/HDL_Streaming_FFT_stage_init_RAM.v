// -------------------------------------------------------------
// 
// File Name: hdl_prj\hdlsrc\OFDM_Transceiver_HDL\HDL_Streaming_FFT_stage_init_RAM.v
// Created: 2014-05-23 20:24:18
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: HDL_Streaming_FFT_stage_init_RAM
// Source Path: OFDM_Transceiver_HDL/Tx/IFFT/HDL Streaming FFT/HDL Streaming FFT_stage_init/HDL Streaming FFT_stage_init_RAM
// Hierarchy Level: 4
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module HDL_Streaming_FFT_stage_init_RAM
          (
           clk,
           enb_1_5_0,
           wr_din_re,
           wr_din_im,
           wr_addr,
           wr_en,
           rd_addr,
           rd_dout_re,
           rd_dout_im
          );


  input   clk;
  input   enb_1_5_0;
  input   signed [15:0] wr_din_re;  // sfix16_En10
  input   signed [15:0] wr_din_im;  // sfix16_En10
  input   [4:0] wr_addr;  // ufix5
  input   wr_en;  // ufix1
  input   [4:0] rd_addr;  // ufix5
  output  signed [15:0] rd_dout_re;  // sfix16_En10
  output  signed [15:0] rd_dout_im;  // sfix16_En10




  SimpleDualPortRAM_32x32b   u_SimpleDualPortRAM_32x32b   (.clk(clk),
                                                           .enb_1_5_0(enb_1_5_0),
                                                           .wr_din_re(wr_din_re),  // sfix16_En10
                                                           .wr_din_im(wr_din_im),  // sfix16_En10
                                                           .wr_addr(wr_addr),  // ufix5
                                                           .wr_en(wr_en),  // ufix1
                                                           .rd_addr(rd_addr),  // ufix5
                                                           .rd_dout_re(rd_dout_re),  // sfix16_En10
                                                           .rd_dout_im(rd_dout_im)  // sfix16_En10
                                                           );

endmodule  // HDL_Streaming_FFT_stage_init_RAM

