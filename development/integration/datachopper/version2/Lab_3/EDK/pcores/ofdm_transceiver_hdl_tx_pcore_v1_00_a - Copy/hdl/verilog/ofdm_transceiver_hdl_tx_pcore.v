// -------------------------------------------------------------
// 
// File Name: OFDM_hdl_prj\hdlsrc\OFDM_Transceiver_HDL\ofdm_transceiver_hdl_tx_pcore.v
// Created: 2014-05-24 20:48:25
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// 
// -- -------------------------------------------------------------
// -- Rate and Clocking Details
// -- -------------------------------------------------------------
// Model base rate: -1
// Target subsystem base rate: -1
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: ofdm_transceiver_hdl_tx_pcore
// Source Path: ofdm_transceiver_hdl_tx_pcore
// Hierarchy Level: 0
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module ofdm_transceiver_hdl_tx_pcore
          (
           IPCORE_CLK,
           IPCORE_RESETN,
           rst_1,
           data_in,
           empty,
           AXI_Lite_ACLK,
           AXI_Lite_ARESETN,
           AXI_Lite_AWADDR,
           AXI_Lite_AWVALID,
           AXI_Lite_WDATA,
           AXI_Lite_WSTRB,
           AXI_Lite_WVALID,
           AXI_Lite_BREADY,
           AXI_Lite_ARADDR,
           AXI_Lite_ARVALID,
           AXI_Lite_RREADY,
           q_out,
           i_out,
           request_byte,
           AXI_Lite_AWREADY,
           AXI_Lite_WREADY,
           AXI_Lite_BRESP,
           AXI_Lite_BVALID,
           AXI_Lite_ARREADY,
           AXI_Lite_RDATA,
           AXI_Lite_RRESP,
           AXI_Lite_RVALID
          );


  input   IPCORE_CLK;  // ufix1
  input   IPCORE_RESETN;  // ufix1
  input   rst_1;  // ufix1
  input   [7:0] data_in;  // ufix8
  input   empty;  // ufix1
  input   AXI_Lite_ACLK;  // ufix1
  input   AXI_Lite_ARESETN;  // ufix1
  input   [31:0] AXI_Lite_AWADDR;  // ufix32
  input   AXI_Lite_AWVALID;  // ufix1
  input   [31:0] AXI_Lite_WDATA;  // ufix32
  input   [3:0] AXI_Lite_WSTRB;  // ufix4
  input   AXI_Lite_WVALID;  // ufix1
  input   AXI_Lite_BREADY;  // ufix1
  input   [31:0] AXI_Lite_ARADDR;  // ufix32
  input   AXI_Lite_ARVALID;  // ufix1
  input   AXI_Lite_RREADY;  // ufix1
  output  [11:0] q_out;  // ufix12
  output  [11:0] i_out;  // ufix12
  output  request_byte;  // ufix1
  output  AXI_Lite_AWREADY;  // ufix1
  output  AXI_Lite_WREADY;  // ufix1
  output  [1:0] AXI_Lite_BRESP;  // ufix2
  output  AXI_Lite_BVALID;  // ufix1
  output  AXI_Lite_ARREADY;  // ufix1
  output  [31:0] AXI_Lite_RDATA;  // ufix32
  output  [1:0] AXI_Lite_RRESP;  // ufix2
  output  AXI_Lite_RVALID;  // ufix1


  wire rst;
  wire reset_cm;  // ufix1
  wire reset_internal;  // ufix1
  wire tx_done_sig;  // ufix1
  wire dut_enable;  // ufix1
  wire clear_fifo_sig;  // ufix1
  wire tx_en_sig;  // ufix1
  wire ce_out_sig;  // ufix1
  wire signed [11:0] q_out_sig;  // sfix12
  wire signed [11:0] i_out_sig;  // sfix12
  wire request_byte_sig;  // ufix1


  assign reset_cm =  ~ IPCORE_RESETN;



  assign rst = reset_cm | reset_internal;



  ofdm_transceiver_hdl_tx_pcore_axi_lite   u_ofdm_transceiver_hdl_tx_pcore_axi_lite_inst   (.rst(rst),
                                                                                            .AXI_Lite_ACLK(AXI_Lite_ACLK),  // ufix1
                                                                                            .AXI_Lite_ARESETN(AXI_Lite_ARESETN),  // ufix1
                                                                                            .AXI_Lite_AWADDR(AXI_Lite_AWADDR),  // ufix32
                                                                                            .AXI_Lite_AWVALID(AXI_Lite_AWVALID),  // ufix1
                                                                                            .AXI_Lite_WDATA(AXI_Lite_WDATA),  // ufix32
                                                                                            .AXI_Lite_WSTRB(AXI_Lite_WSTRB),  // ufix4
                                                                                            .AXI_Lite_WVALID(AXI_Lite_WVALID),  // ufix1
                                                                                            .AXI_Lite_BREADY(AXI_Lite_BREADY),  // ufix1
                                                                                            .AXI_Lite_ARADDR(AXI_Lite_ARADDR),  // ufix32
                                                                                            .AXI_Lite_ARVALID(AXI_Lite_ARVALID),  // ufix1
                                                                                            .AXI_Lite_RREADY(AXI_Lite_RREADY),  // ufix1
                                                                                            .read_tx_done(tx_done_sig),  // ufix1
                                                                                            .AXI_Lite_AWREADY(AXI_Lite_AWREADY),  // ufix1
                                                                                            .AXI_Lite_WREADY(AXI_Lite_WREADY),  // ufix1
                                                                                            .AXI_Lite_BRESP(AXI_Lite_BRESP),  // ufix2
                                                                                            .AXI_Lite_BVALID(AXI_Lite_BVALID),  // ufix1
                                                                                            .AXI_Lite_ARREADY(AXI_Lite_ARREADY),  // ufix1
                                                                                            .AXI_Lite_RDATA(AXI_Lite_RDATA),  // ufix32
                                                                                            .AXI_Lite_RRESP(AXI_Lite_RRESP),  // ufix2
                                                                                            .AXI_Lite_RVALID(AXI_Lite_RVALID),  // ufix1
                                                                                            .write_axi_enable(dut_enable),  // ufix1
                                                                                            .write_clear_fifo(clear_fifo_sig),  // ufix1
                                                                                            .write_tx_en(tx_en_sig),  // ufix1
                                                                                            .reset_internal(reset_internal)  // ufix1
                                                                                            );

  ofdm_transceiver_hdl_tx_pcore_dut   u_ofdm_transceiver_hdl_tx_pcore_dut_inst   (.clk(IPCORE_CLK),  // ufix1
                                                                                  .rst(rst),
                                                                                  .dut_enable(dut_enable),  // ufix1
                                                                                  .rst_1(rst_1),  // ufix1
                                                                                  .data_in(data_in),  // ufix8
                                                                                  .tx_en(tx_en_sig),  // ufix1
                                                                                  .empty(empty),  // ufix1
                                                                                  .clear_fifo(clear_fifo_sig),  // ufix1
                                                                                  .ce_out(ce_out_sig),  // ufix1
                                                                                  .q_out(q_out_sig),  // sfix12
                                                                                  .i_out(i_out_sig),  // sfix12
                                                                                  .tx_done(tx_done_sig),  // ufix1
                                                                                  .request_byte(request_byte_sig)  // ufix1
                                                                                  );

  assign q_out = q_out_sig;

  assign i_out = i_out_sig;

  assign request_byte = request_byte_sig;

endmodule  // ofdm_transceiver_hdl_tx_pcore

