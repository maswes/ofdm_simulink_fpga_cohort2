clear; clear all; clear set;

fid = fopen('h.prn','r');
M = textscan(fid, '%d	%d	%d	%d	%d	%d	%d	%d	%d	%d	%s	%d	%d','Headerlines', 1);
fclose(fid);

output = M{12};
pop = M{13};
ready = M{5};
u = unique(output);
x = u(histc(output,u)>=1);

hasChange = 0;
currentValue = 0;
count = 0;

data_index = 1;
min_count = 100000000000;
for k=1:length(output);
    if k==1
        currentValue = output(k);
    else
        if output(k)== currentValue
            count = count + 1;
        else
            if(count < min_count)
                min_count = count;
            end
            data_array(data_index,1) = currentValue;
            data_array(data_index,2) = count;
            data_index = data_index + 1;
            count = 1;
            currentValue = output(k);
        end
    end
end

sequence_index = 1;
for k=2:length(data_array)
    current_count = data_array(k,2);

    while current_count > min_count;
        sequence_data(sequence_index) = data_array(k,1);
        sequence_index = sequence_index + 1;
        current_count = current_count - min_count;
    end
    sequence_data(sequence_index) = data_array(k,1);
    sequence_index = sequence_index + 1;
end

sequence_data = sequence_data;
