// -------------------------------------------------------------
// 
// File Name: OFDM_hdl_prj\hdlsrc\OFDM_Transceiver_HDL\DualPortRAM_32x32b.v
// Created: 2014-05-22 15:35:40
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: DualPortRAM_32x32b
// Source Path: OFDM_Transceiver_HDL/Tx/Insert_CyclicPrefix/DualPortRAM_Inst0/DualPortRAM_32x32b
// Hierarchy Level: 3
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module DualPortRAM_32x32b
          (
           clk,
           enb,
           wr_din_re,
           wr_din_im,
           wr_addr,
           wr_en,
           rd_addr,
           wr_dout_re,
           wr_dout_im,
           rd_dout_re,
           rd_dout_im
          );


  input   clk;
  input   enb;
  input   signed [15:0] wr_din_re;  // sfix16_En10
  input   signed [15:0] wr_din_im;  // sfix16_En10
  input   [4:0] wr_addr;  // ufix5
  input   wr_en;  // ufix1
  input   [4:0] rd_addr;  // ufix5
  output  signed [15:0] wr_dout_re;  // sfix16_En10
  output  signed [15:0] wr_dout_im;  // sfix16_En10
  output  signed [15:0] rd_dout_re;  // sfix16_En10
  output  signed [15:0] rd_dout_im;  // sfix16_En10

  parameter addr_width = 3'd5;
  parameter data_width = 6'd32;

  reg  [data_width - 1:0] ram [2**addr_width - 1:0];
  reg  [data_width - 1:0] dout_b;
  reg  [data_width - 1:0] dout_a;
  integer i;

  initial begin
    for (i=0; i<=2**addr_width - 1; i=i+1) begin
      ram[i] = 0;
    end
    dout_b = 0;
    dout_a = 0;
  end


  always @(posedge clk)
    begin : DualPortRAM_32x32b_process
      if (enb == 1'b1) begin
        if (wr_en == 1'b1) begin
          ram[wr_addr] <= {wr_din_re, wr_din_im};
          dout_a <= {wr_din_re, wr_din_im};
        end
        else begin
          dout_a <= ram[wr_addr];
        end
        dout_b <= ram[rd_addr];
      end
    end

  assign rd_dout_re = dout_b[data_width-1:data_width/2];
  assign rd_dout_im = dout_b[data_width/2-1:0];
  assign wr_dout_re = dout_a[data_width-1:data_width/2];
  assign wr_dout_im = dout_a[data_width/2-1:0];

endmodule  // DualPortRAM_32x32b

