#ChipScope Core Generator Project File Version 3.0
#Thu May 22 01:17:20 Pacific Daylight Time 2014
SignalExport.bus<0000>.channelList=0 1 2 3 4 5 6 7 8
SignalExport.bus<0000>.name=TRIG0
SignalExport.bus<0000>.offset=0.0
SignalExport.bus<0000>.precision=0
SignalExport.bus<0000>.radix=Bin
SignalExport.bus<0000>.scaleFactor=1.0
SignalExport.bus<0001>.channelList= 8 7 6 5 4 3 2 1
SignalExport.bus<0001>.name=dout
SignalExport.bus<0001>.offset=0.0
SignalExport.bus<0001>.precision=0
SignalExport.bus<0001>.radix=Hex
SignalExport.bus<0001>.scaleFactor=1.0
SignalExport.clockChannel=CLK
SignalExport.dataEqualsTrigger=true
SignalExport.triggerChannel<0000><0000>=empty
SignalExport.triggerChannel<0000><0001>=dout[0]
SignalExport.triggerChannel<0000><0002>=dout[1]
SignalExport.triggerChannel<0000><0003>=dout[2]
SignalExport.triggerChannel<0000><0004>=dout[3]
SignalExport.triggerChannel<0000><0005>=dout[4]
SignalExport.triggerChannel<0000><0006>=dout[5]
SignalExport.triggerChannel<0000><0007>=dout[6]
SignalExport.triggerChannel<0000><0008>=dout[7]
SignalExport.triggerPort<0000>.name=TRIG0
SignalExport.triggerPortCount=1
SignalExport.triggerPortIsData<0000>=true
SignalExport.triggerPortWidth<0000>=9
SignalExport.type=ila


