#################################################################
# Makefile generated by Xilinx Platform Studio 
# Project:C:\Personal\School\Lab_3\EDK_Original\system.xmp
#
# WARNING : This file will be re-generated every time a command
# to run a make target is invoked. So, any changes made to this  
# file manually, will be lost when make is invoked next. 
#################################################################

SHELL = CMD

XILINX_EDK_DIR = C:/Xilinx/14.7/ISE_DS/EDK

SYSTEM = system

MHSFILE = system.mhs

PCWPRJFILE = data/ps7_system_prj.xml

FPGA_ARCH = zynq

DEVICE = xc7z020clg484-1

INTSTYLE = default

XPS_HDL_LANG = vhdl
GLOBAL_SEARCHPATHOPT = 
PROJECT_SEARCHPATHOPT = 

SEARCHPATHOPT = $(PROJECT_SEARCHPATHOPT) $(GLOBAL_SEARCHPATHOPT)

SUBMODULE_OPT = 

PLATGEN_OPTIONS = -p $(DEVICE) -lang $(XPS_HDL_LANG) -intstyle $(INTSTYLE) $(SEARCHPATHOPT) $(SUBMODULE_OPT) -msg __xps/ise/xmsgprops.lst

OBSERVE_PAR_OPTIONS = -error yes

MICROBLAZE_BOOTLOOP = $(XILINX_EDK_DIR)/sw/lib/microblaze/mb_bootloop.elf
MICROBLAZE_BOOTLOOP_LE = $(XILINX_EDK_DIR)/sw/lib/microblaze/mb_bootloop_le.elf
PPC405_BOOTLOOP = $(XILINX_EDK_DIR)/sw/lib/ppc405/ppc_bootloop.elf
PPC440_BOOTLOOP = $(XILINX_EDK_DIR)/sw/lib/ppc440/ppc440_bootloop.elf
BOOTLOOP_DIR = bootloops

BRAMINIT_ELF_IMP_FILES =
BRAMINIT_ELF_IMP_FILE_ARGS =

BRAMINIT_ELF_SIM_FILES =
BRAMINIT_ELF_SIM_FILE_ARGS =

SIM_CMD = isim_system

BEHAVIORAL_SIM_SCRIPT = simulation/behavioral/$(SYSTEM)_setup.tcl

STRUCTURAL_SIM_SCRIPT = simulation/structural/$(SYSTEM)_setup.tcl

TIMING_SIM_SCRIPT = simulation/timing/$(SYSTEM)_setup.tcl

DEFAULT_SIM_SCRIPT = $(BEHAVIORAL_SIM_SCRIPT)

SIMGEN_OPTIONS = -p $(DEVICE) -lang $(XPS_HDL_LANG) -intstyle $(INTSTYLE) $(SEARCHPATHOPT) $(BRAMINIT_ELF_SIM_FILE_ARGS) -msg __xps/ise/xmsgprops.lst -s isim -X C:/Personal/School/Lab_3/EDK_Original/


CORE_STATE_DEVELOPMENT_FILES = pcores/dac_driver_pcore_v1_00_a/hdl/vhdl/dac_driver_fixpt.vhd \
pcores/dac_driver_pcore_v1_00_a/hdl/vhdl/dac_driver_pcore_dut.vhd \
pcores/dac_driver_pcore_v1_00_a/hdl/vhdl/dac_driver_pcore_axi_lite_module.vhd \
pcores/dac_driver_pcore_v1_00_a/hdl/vhdl/dac_driver_pcore_addr_decoder.vhd \
pcores/dac_driver_pcore_v1_00_a/hdl/vhdl/dac_driver_pcore_axi_lite.vhd \
pcores/dac_driver_pcore_v1_00_a/hdl/vhdl/dac_driver_pcore.vhd \
pcores/mcu_driver_pcore_v1_00_a/hdl/vhdl/mcu_driver_fixpt.vhd \
pcores/mcu_driver_pcore_v1_00_a/hdl/vhdl/mcu_driver_pcore_dut.vhd \
pcores/mcu_driver_pcore_v1_00_a/hdl/vhdl/mcu_driver_pcore_axi_lite_module.vhd \
pcores/mcu_driver_pcore_v1_00_a/hdl/vhdl/mcu_driver_pcore_addr_decoder.vhd \
pcores/mcu_driver_pcore_v1_00_a/hdl/vhdl/mcu_driver_pcore_axi_lite.vhd \
pcores/mcu_driver_pcore_v1_00_a/hdl/vhdl/mcu_driver_pcore.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/SimpleDualPortRAM_2048x8b.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/fifo_ram.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/tx_fifo_fixpt.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/tx_fifo_pcore_dut.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/tx_fifo_pcore_axi_lite_module.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/tx_fifo_pcore_addr_decoder.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/tx_fifo_pcore_axi_lite.vhd \
pcores/tx_fifo_pcore_v1_00_a/hdl/vhdl/tx_fifo_pcore.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/qpsk_tx_fixpt_pkg.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/SimpleDualPortRAM_1024x8b.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/tx_fifo_ram.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/qpsk_tx_fixpt.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/qpsk_tx_pcore_dut.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/qpsk_tx_pcore_axi_lite_module.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/qpsk_tx_pcore_addr_decoder.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/qpsk_tx_pcore_axi_lite.vhd \
pcores/qpsk_tx_pcore_v1_00_a/hdl/vhdl/qpsk_tx_pcore.vhd

WRAPPER_NGC_FILES = implementation/system_tx_clock_generator_wrapper.ngc \
implementation/system_processing_system7_0_wrapper.ngc \
implementation/system_axi_gpio_button_wrapper.ngc \
implementation/system_axi_interconnect_1_wrapper.ngc \
implementation/system_axi_gpio_led_wrapper.ngc \
implementation/system_dac_driver_wrapper.ngc \
implementation/system_mcu_driver_wrapper.ngc \
implementation/system_tx_fifo_wrapper.ngc \
implementation/system_qpsk_tx_wrapper.ngc \
implementation/system_axi_gpio_switch_wrapper.ngc \
implementation/system_mcu_uart_wrapper.ngc \
implementation/system_chipscope_ila_0_wrapper.ngc \
implementation/system_chipscope_icon_0_wrapper.ngc \
implementation/system_chipscope_ila_1_wrapper.ngc

POSTSYN_NETLIST = implementation/$(SYSTEM).ngc

SYSTEM_BIT = implementation/$(SYSTEM).bit

DOWNLOAD_BIT = implementation/download.bit

SYSTEM_ACE = implementation/$(SYSTEM).ace

UCF_FILE = data\system.ucf

BMM_FILE = implementation/$(SYSTEM).bmm

BITGEN_UT_FILE = etc/bitgen.ut

XFLOW_OPT_FILE = etc/fast_runtime.opt
XFLOW_DEPENDENCY = __xps/xpsxflow.opt $(XFLOW_OPT_FILE)

XPLORER_DEPENDENCY = __xps/xplorer.opt
XPLORER_OPTIONS = -p $(DEVICE) -uc $(SYSTEM).ucf -bm $(SYSTEM).bmm -max_runs 7

FPGA_IMP_DEPENDENCY = $(BMM_FILE) $(POSTSYN_NETLIST) $(UCF_FILE) $(XFLOW_DEPENDENCY)

SDK_EXPORT_DIR = SDK\SDK_Export\hw
SYSTEM_HW_HANDOFF = $(SDK_EXPORT_DIR)/$(SYSTEM).xml
SYSTEM_HW_HANDOFF_BIT = $(SDK_EXPORT_DIR)/$(SYSTEM).bit
SYSTEM_HW_HANDOFF_DEP = $(SYSTEM_HW_HANDOFF) $(SYSTEM_HW_HANDOFF_BIT)
