// -------------------------------------------------------------
// 
// File Name: hdl_prj\hdlsrc\OFDM_Transceiver_HDL\HDL_Streaming_FFT_stage_2.v
// Created: 2014-05-31 16:15:43
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: HDL_Streaming_FFT_stage_2
// Source Path: OFDM_TX/IFFT/HDL Streaming FFT/HDL Streaming FFT_stage_2
// Hierarchy Level: 3
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module HDL_Streaming_FFT_stage_2
          (
           clk,
           rst,
           enb_1_30_0,
           u_re,
           u_im,
           v_re,
           v_im,
           enb_in,
           bf1_re,
           bf1_im,
           bf2_re,
           bf2_im,
           enb_out
          );


  input   clk;
  input   rst;
  input   enb_1_30_0;
  input   signed [15:0] u_re;  // sfix16_En14
  input   signed [15:0] u_im;  // sfix16_En14
  input   signed [15:0] v_re;  // sfix16_En14
  input   signed [15:0] v_im;  // sfix16_En14
  input   enb_in;  // ufix1
  output  signed [15:0] bf1_re;  // sfix16_En14
  output  signed [15:0] bf1_im;  // sfix16_En14
  output  signed [15:0] bf2_re;  // sfix16_En14
  output  signed [15:0] bf2_im;  // sfix16_En14
  output  enb_out;  // ufix1

  parameter signed [10:0] table_data_re_0 = 1023;  // sfix11
  parameter signed [10:0] table_data_re_1 = 724;  // sfix11
  parameter signed [10:0] table_data_re_2 = 0;  // sfix11
  parameter signed [10:0] table_data_re_3 = -724;  // sfix11
  parameter signed [10:0] table_data_im_0 = 0;  // sfix11
  parameter signed [10:0] table_data_im_1 = -724;  // sfix11
  parameter signed [10:0] table_data_im_2 = -1024;  // sfix11
  parameter signed [10:0] table_data_im_3 = -724;  // sfix11

  reg [1:0] tc_count;  // ufix2
  wire [1:0] taddr;  // ufix2
  wire shuffle_c;  // ufix1
  reg  [0:3] pipe_shuffle_c_reg;  // ufix1 [4]
  wire [0:3] pipe_shuffle_c_reg_next;  // ufix1 [4]
  wire shuffle_c_p;  // ufix1
  wire signed [10:0] twiddle_rom_table_data_re [0:3];  // sfix11_En10 [4]
  wire signed [10:0] twiddle_rom_table_data_im [0:3];  // sfix11_En10 [4]
  wire signed [10:0] twiddle_re;  // sfix11_En10
  wire signed [10:0] twiddle_im;  // sfix11_En10
  reg signed [10:0] twiddle_out_re;  // sfix11_En10
  reg signed [10:0] twiddle_out_im;  // sfix11_En10
  wire twiddleone;  // ufix1
  wire signed [15:0] x_re;  // sfix16_En14
  wire signed [15:0] x_im;  // sfix16_En14
  wire signed [15:0] y_re;  // sfix16_En14
  wire signed [15:0] y_im;  // sfix16_En14
  reg signed [15:0] x_pc_reg_re [0:1];  // sfix16_En14 [2]
  reg signed [15:0] x_pc_reg_im [0:1];  // sfix16_En14 [2]
  wire signed [15:0] x_pc_reg_next_re [0:1];  // sfix16_En14 [2]
  wire signed [15:0] x_pc_reg_next_im [0:1];  // sfix16_En14 [2]
  wire signed [15:0] x_p_re;  // sfix16_En14
  wire signed [15:0] x_p_im;  // sfix16_En14
  reg signed [15:0] y_d_re;  // sfix16_En14
  reg signed [15:0] y_d_im;  // sfix16_En14
  wire signed [15:0] sw1_re;  // sfix16_En14
  wire signed [15:0] sw1_im;  // sfix16_En14
  reg signed [15:0] sr2_reg_re [0:1];  // sfix16_En14 [2]
  reg signed [15:0] sr2_reg_im [0:1];  // sfix16_En14 [2]
  wire signed [15:0] sr2_reg_next_re [0:1];  // sfix16_En14 [2]
  wire signed [15:0] sr2_reg_next_im [0:1];  // sfix16_En14 [2]
  reg  [0:3] pipe_enb_reg;  // ufix1 [4]
  wire [0:3] pipe_enb_reg_next;  // ufix1 [4]
  wire enb_p;  // ufix1
  reg  [0:1] sr3_reg;  // ufix1 [2]
  wire [0:1] sr3_reg_next;  // ufix1 [2]


  // Free running, Unsigned Counter
  //  initial value   = 0
  //  step value      = 1
  // 
  // Twiddle ROM address counter
  always @(posedge clk or posedge rst)
    begin : tc_process
      if (rst == 1'b1) begin
        tc_count <= 2'b00;
      end
      else begin
        if (enb_1_30_0 && enb_in) begin
          tc_count <= tc_count + 1;
        end
      end
    end

  assign taddr = tc_count;



  // Shuffling unit control signal
  assign shuffle_c = taddr[1];



  // Matching pipelining delays on shuffle control signal
  always @(posedge clk or posedge rst)
    begin : pipe_shuffle_c_process
      if (rst == 1'b1) begin
        pipe_shuffle_c_reg[0] <= 1'b0;
        pipe_shuffle_c_reg[1] <= 1'b0;
        pipe_shuffle_c_reg[2] <= 1'b0;
        pipe_shuffle_c_reg[3] <= 1'b0;
      end
      else begin
        if (enb_1_30_0) begin
          pipe_shuffle_c_reg[0] <= pipe_shuffle_c_reg_next[0];
          pipe_shuffle_c_reg[1] <= pipe_shuffle_c_reg_next[1];
          pipe_shuffle_c_reg[2] <= pipe_shuffle_c_reg_next[2];
          pipe_shuffle_c_reg[3] <= pipe_shuffle_c_reg_next[3];
        end
      end
    end

  assign shuffle_c_p = pipe_shuffle_c_reg[3];
  assign pipe_shuffle_c_reg_next[0] = shuffle_c;
  assign pipe_shuffle_c_reg_next[1] = pipe_shuffle_c_reg[0];
  assign pipe_shuffle_c_reg_next[2] = pipe_shuffle_c_reg[1];
  assign pipe_shuffle_c_reg_next[3] = pipe_shuffle_c_reg[2];



  // Twiddle ROM
  assign twiddle_rom_table_data_re[0] = table_data_re_0;
  assign twiddle_rom_table_data_re[1] = table_data_re_1;
  assign twiddle_rom_table_data_re[2] = table_data_re_2;
  assign twiddle_rom_table_data_re[3] = table_data_re_3;
  assign twiddle_rom_table_data_im[0] = table_data_im_0;
  assign twiddle_rom_table_data_im[1] = table_data_im_1;
  assign twiddle_rom_table_data_im[2] = table_data_im_2;
  assign twiddle_rom_table_data_im[3] = table_data_im_3;
  assign twiddle_re = twiddle_rom_table_data_re[taddr];
  assign twiddle_im = twiddle_rom_table_data_im[taddr];



  always @(posedge clk or posedge rst)
    begin : twiddle_outc_process
      if (rst == 1'b1) begin
        twiddle_out_re <= 11'sb00000000000;
        twiddle_out_im <= 11'sb00000000000;
      end
      else begin
        if (enb_1_30_0) begin
          twiddle_out_re <= twiddle_re;
          twiddle_out_im <= twiddle_im;
        end
      end
    end



  // Twiddle equal to one control signal
  assign twiddleone = (taddr == 2'b00 ? 1'b1 :
              1'b0);



  // DIF FFT butterfly unit
  HDL_Streaming_FFT_Butterfly_DIF_Unit   u_butterfly_DIF_inst   (.clk(clk),
                                                                 .rst(rst),
                                                                 .enb_1_30_0(enb_1_30_0),
                                                                 .u_re(u_re),  // sfix16_En14
                                                                 .u_im(u_im),  // sfix16_En14
                                                                 .v_re(v_re),  // sfix16_En14
                                                                 .v_im(v_im),  // sfix16_En14
                                                                 .twiddle_re(twiddle_out_re),  // sfix11_En10
                                                                 .twiddle_im(twiddle_out_im),  // sfix11_En10
                                                                 .twiddleone(twiddleone),  // ufix1
                                                                 .x_out_re(x_re),  // sfix16_En14
                                                                 .x_out_im(x_im),  // sfix16_En14
                                                                 .y_out_re(y_re),  // sfix16_En14
                                                                 .y_out_im(y_im)  // sfix16_En14
                                                                 );

  always @(posedge clk or posedge rst)
    begin : x_pc_process
      if (rst == 1'b1) begin
        x_pc_reg_re[0] <= 16'sb0000000000000000;
        x_pc_reg_im[0] <= 16'sb0000000000000000;
        x_pc_reg_re[1] <= 16'sb0000000000000000;
        x_pc_reg_im[1] <= 16'sb0000000000000000;
      end
      else begin
        if (enb_1_30_0) begin
          x_pc_reg_re[0] <= x_pc_reg_next_re[0];
          x_pc_reg_im[0] <= x_pc_reg_next_im[0];
          x_pc_reg_re[1] <= x_pc_reg_next_re[1];
          x_pc_reg_im[1] <= x_pc_reg_next_im[1];
        end
      end
    end

  assign x_p_re = x_pc_reg_re[1];
  assign x_p_im = x_pc_reg_im[1];
  assign x_pc_reg_next_re[0] = x_re;
  assign x_pc_reg_next_im[0] = x_im;
  assign x_pc_reg_next_re[1] = x_pc_reg_re[0];
  assign x_pc_reg_next_im[1] = x_pc_reg_im[0];



  // Shift register sr1
  always @(posedge clk or posedge rst)
    begin : sr1_process
      if (rst == 1'b1) begin
        y_d_re <= 16'sb0000000000000000;
        y_d_im <= 16'sb0000000000000000;
      end
      else begin
        if (enb_1_30_0) begin
          y_d_re <= y_re;
          y_d_im <= y_im;
        end
      end
    end



  // Shuffle unit switch
  assign sw1_re = (shuffle_c_p == 1'b0 ? x_p_re :
              y_d_re);
  assign sw1_im = (shuffle_c_p == 1'b0 ? x_p_im :
              y_d_im);



  // Shift register sr2
  always @(posedge clk or posedge rst)
    begin : sr2_process
      if (rst == 1'b1) begin
        sr2_reg_re[0] <= 16'sb0000000000000000;
        sr2_reg_im[0] <= 16'sb0000000000000000;
        sr2_reg_re[1] <= 16'sb0000000000000000;
        sr2_reg_im[1] <= 16'sb0000000000000000;
      end
      else begin
        if (enb_1_30_0) begin
          sr2_reg_re[0] <= sr2_reg_next_re[0];
          sr2_reg_im[0] <= sr2_reg_next_im[0];
          sr2_reg_re[1] <= sr2_reg_next_re[1];
          sr2_reg_im[1] <= sr2_reg_next_im[1];
        end
      end
    end

  assign bf1_re = sr2_reg_re[1];
  assign bf1_im = sr2_reg_im[1];
  assign sr2_reg_next_re[0] = sw1_re;
  assign sr2_reg_next_im[0] = sw1_im;
  assign sr2_reg_next_re[1] = sr2_reg_re[0];
  assign sr2_reg_next_im[1] = sr2_reg_im[0];



  // Shuffle unit switch
  assign bf2_re = (shuffle_c_p == 1'b0 ? y_d_re :
              x_p_re);
  assign bf2_im = (shuffle_c_p == 1'b0 ? y_d_im :
              x_p_im);



  // Matching pipelining delays on enable signal
  always @(posedge clk or posedge rst)
    begin : pipe_enb_process
      if (rst == 1'b1) begin
        pipe_enb_reg[0] <= 1'b0;
        pipe_enb_reg[1] <= 1'b0;
        pipe_enb_reg[2] <= 1'b0;
        pipe_enb_reg[3] <= 1'b0;
      end
      else begin
        if (enb_1_30_0) begin
          pipe_enb_reg[0] <= pipe_enb_reg_next[0];
          pipe_enb_reg[1] <= pipe_enb_reg_next[1];
          pipe_enb_reg[2] <= pipe_enb_reg_next[2];
          pipe_enb_reg[3] <= pipe_enb_reg_next[3];
        end
      end
    end

  assign enb_p = pipe_enb_reg[3];
  assign pipe_enb_reg_next[0] = enb_in;
  assign pipe_enb_reg_next[1] = pipe_enb_reg[0];
  assign pipe_enb_reg_next[2] = pipe_enb_reg[1];
  assign pipe_enb_reg_next[3] = pipe_enb_reg[2];



  // Matching pipelining delays on enable signal
  always @(posedge clk or posedge rst)
    begin : sr3_process
      if (rst == 1'b1) begin
        sr3_reg[0] <= 1'b0;
        sr3_reg[1] <= 1'b0;
      end
      else begin
        if (enb_1_30_0) begin
          sr3_reg[0] <= sr3_reg_next[0];
          sr3_reg[1] <= sr3_reg_next[1];
        end
      end
    end

  assign enb_out = sr3_reg[1];
  assign sr3_reg_next[0] = enb_p;
  assign sr3_reg_next[1] = sr3_reg[0];



endmodule  // HDL_Streaming_FFT_stage_2

