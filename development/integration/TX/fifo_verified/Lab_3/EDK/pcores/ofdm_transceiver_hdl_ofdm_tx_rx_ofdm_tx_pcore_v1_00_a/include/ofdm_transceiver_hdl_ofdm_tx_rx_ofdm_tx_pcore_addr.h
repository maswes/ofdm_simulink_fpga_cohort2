/*
 * File Name:         hdl_prj\ipcore\ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_v1_00_a\include\ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_addr.h
 * Description:       C Header File
 * Created:           2014-05-31 16:15:49
*/

#ifndef OFDM_TRANSCEIVER_HDL_OFDM_TX_RX_OFDM_TX_PCORE_H_
#define OFDM_TRANSCEIVER_HDL_OFDM_TX_RX_OFDM_TX_PCORE_H_

#define  IPCore_Reset_ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore      0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore     0x4  //enabled (by default) when bit 0 is 0x1
#define  clear_fifo_Data_ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore   0x100  //data register for port clear_fifo
#define  tx_en_Data_ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore        0x104  //data register for port tx_en
#define  tx_done_Data_ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore      0x108  //data register for port tx_done

#endif /* OFDM_TRANSCEIVER_HDL_OFDM_TX_RX_OFDM_TX_PCORE_H_ */
