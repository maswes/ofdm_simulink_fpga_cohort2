// -------------------------------------------------------------
// 
// File Name: hdl_prj\hdlsrc\OFDM_Transceiver_HDL\ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_addr_decoder.v
// Created: 2014-05-31 16:15:48
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_addr_decoder
// Source Path: ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore/ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_axi_lite/ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_addr_decoder
// Hierarchy Level: 2
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_addr_decoder
          (
           clk,
           rst,
           data_write,
           addr_sel,
           wr_enb,
           rd_enb,
           read_tx_done,
           data_read,
           write_axi_enable,
           write_clear_fifo,
           write_tx_en
          );


  input   clk;
  input   rst;
  input   [31:0] data_write;  // ufix32
  input   [13:0] addr_sel;  // ufix14
  input   wr_enb;  // ufix1
  input   rd_enb;  // ufix1
  input   read_tx_done;  // ufix1
  output  [31:0] data_read;  // ufix32
  output  write_axi_enable;  // ufix1
  output  write_clear_fifo;  // ufix1
  output  write_tx_en;  // ufix1


  wire enb;
  wire decode_sel_tx_done;  // ufix1
  wire const_1;  // ufix1
  wire [31:0] const_z;  // ufix32
  reg  read_reg_tx_done;  // ufix1
  wire [31:0] data_in_tx_done;  // ufix32
  wire [31:0] decode_rd_tx_done;  // ufix32
  wire data_in_axi_enable;  // ufix1
  wire decode_sel_axi_enable;  // ufix1
  wire reg_enb_axi_enable;  // ufix1
  reg  write_reg_axi_enable;  // ufix1
  wire data_in_clear_fifo;  // ufix1
  wire decode_sel_clear_fifo;  // ufix1
  wire reg_enb_clear_fifo;  // ufix1
  reg  write_reg_clear_fifo;  // ufix1
  wire data_in_tx_en;  // ufix1
  wire decode_sel_tx_en;  // ufix1
  wire reg_enb_tx_en;  // ufix1
  reg  write_reg_tx_en;  // ufix1


  assign decode_sel_tx_done = (addr_sel == 14'b00000001000010 ? 1'b1 :
              1'b0);



  assign const_1 = 1'b1;



  assign enb = const_1;

  assign const_z = 32'bz;


  always @(posedge clk or posedge rst)
    begin : reg_tx_done_process
      if (rst == 1'b1) begin
        read_reg_tx_done <= 1'b0;
      end
      else begin
        if (enb) begin
          read_reg_tx_done <= read_tx_done;
        end
      end
    end



  assign data_in_tx_done = {31'b0, read_reg_tx_done};



  assign decode_rd_tx_done = (decode_sel_tx_done == 1'b0 ? const_z :
              data_in_tx_done);



  assign data_read = decode_rd_tx_done;

  assign data_in_axi_enable = data_write[0];



  assign decode_sel_axi_enable = (addr_sel == 14'b00000000000001 ? 1'b1 :
              1'b0);



  assign reg_enb_axi_enable = decode_sel_axi_enable & wr_enb;



  always @(posedge clk or posedge rst)
    begin : reg_axi_enable_process
      if (rst == 1'b1) begin
        write_reg_axi_enable <= 1'b1;
      end
      else begin
        if (enb && reg_enb_axi_enable) begin
          write_reg_axi_enable <= data_in_axi_enable;
        end
      end
    end



  assign write_axi_enable = write_reg_axi_enable;

  assign data_in_clear_fifo = data_write[0];



  assign decode_sel_clear_fifo = (addr_sel == 14'b00000001000000 ? 1'b1 :
              1'b0);



  assign reg_enb_clear_fifo = decode_sel_clear_fifo & wr_enb;



  always @(posedge clk or posedge rst)
    begin : reg_clear_fifo_process
      if (rst == 1'b1) begin
        write_reg_clear_fifo <= 1'b0;
      end
      else begin
        if (enb && reg_enb_clear_fifo) begin
          write_reg_clear_fifo <= data_in_clear_fifo;
        end
      end
    end



  assign write_clear_fifo = write_reg_clear_fifo;

  assign data_in_tx_en = data_write[0];



  assign decode_sel_tx_en = (addr_sel == 14'b00000001000001 ? 1'b1 :
              1'b0);



  assign reg_enb_tx_en = decode_sel_tx_en & wr_enb;



  always @(posedge clk or posedge rst)
    begin : reg_tx_en_process
      if (rst == 1'b1) begin
        write_reg_tx_en <= 1'b0;
      end
      else begin
        if (enb && reg_enb_tx_en) begin
          write_reg_tx_en <= data_in_tx_en;
        end
      end
    end



  assign write_tx_en = write_reg_tx_en;

endmodule  // ofdm_transceiver_hdl_ofdm_tx_rx_ofdm_tx_pcore_addr_decoder

